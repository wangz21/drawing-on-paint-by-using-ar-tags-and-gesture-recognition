# -*- coding: UTF-8 -*- #
"""
@fileName:perspective_transform.py
@author:Zicheng Wang
@time:2023-12-18
"""
import cv2
import numpy as np
import pyautogui

smooth = 2  # Smooth parameter


def perspective_transformation(image, corners, x1, y1, p_x, p_y, draw, paint_content):
    # Convert the source points to a float32 numpy array
    corners = np.array(corners, dtype=np.float32)

    destination_point = np.array([
        [0, 0], [image.shape[1] - 1, 0], [image.shape[1] - 1, image.shape[0] - 1], [0, image.shape[0] - 1]],
        dtype=np.float32)

    # Transformation matrix
    transform_matrix = cv2.getPerspectiveTransform(corners, destination_point)

    # Perform the perspective transformation, keeping the image size the same
    transformed_image = cv2.warpPerspective(image, transform_matrix, (image.shape[1], image.shape[0]))

    if x1 != 0 and y1 != 0:
        # If the finger is detected, apply smoothing and update the point
        fingertip_point = np.array([[[x1, y1]]], dtype=np.float32)
        transformed_fingertip_point = cv2.perspectiveTransform(fingertip_point, transform_matrix)
        # Get the transformed coordinates
        transformed_x, transformed_y = transformed_fingertip_point[0][0]

        # Apply the smoothing algorithm to stabilize the point
        new_p_x = p_x + (transformed_x - p_x) / smooth
        new_p_y = p_y + (transformed_y - p_y) / smooth

        # Draw the smoothed point
        cv2.circle(transformed_image, (int(new_p_x), int(new_p_y)), 10, (0, 0, 255), cv2.FILLED)

        paint_x, paint_y, paint_w, paint_h = paint_content[0]

        # Calculate the related position
        relative_x = new_p_x / image.shape[1]
        relative_y = new_p_y / image.shape[0]

        mapped_x = paint_x + relative_x * paint_w
        mapped_y = paint_y + relative_y * paint_h

        pyautogui.moveTo(mapped_x, mapped_y)
        if draw == 1:
            pyautogui.mouseDown(mapped_x, mapped_y, button="left")
        else:
            pyautogui.mouseUp(button="left")

        # Update the previous coordinates for the next frame
        p_x, p_y = new_p_x, new_p_y
    else:
        # If no finger is detected, don't update p_x & p_y
        if p_x != 0 and p_y != 0:
            # Draw the existing point
            cv2.circle(transformed_image, (int(p_x), int(p_y)), 10, (0, 0, 255), cv2.FILLED)

    return transformed_image, p_x, p_y
