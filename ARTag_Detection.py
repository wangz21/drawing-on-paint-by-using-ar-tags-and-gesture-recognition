# -*- coding: UTF-8 -*- #
"""
@fileName: ARTag_Detection.py
@author:Zicheng Wang
@time: 2023-11-09
"""

# Don't try to update the known_position everytime.
# Because it will make the system become lagging 90% memory consumption
import numpy as np
import cv2
from cv2 import aruco
from cvzone.HandTrackingModule import HandDetector
import estimate_position

# Initialize the AR Tag detector
aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_ARUCO_ORIGINAL)
parameters = aruco.DetectorParameters()
detector = aruco.ArucoDetector(aruco_dict, parameters)
# Hand detector
hand_detector = HandDetector(staticMode=False, maxHands=1, detectionCon=0.8, minTrackCon=0.5)

# Global variable for storing known positions
known_positions_global = {}


def is_finger_inside_quad(image, quad_points, hand_detector):
    hands, image = hand_detector.findHands(image, flipType=False)
    if hands:
        # Get the index fingertip position
        lmList = hands[0]['lmList']

        x1, y1, _ = lmList[8]

        # Create a mask for the image
        mask = np.zeros(image.shape[:2], dtype=np.uint8)
        if 0 <= x1 < mask.shape[1] and 0 <= y1 < mask.shape[0]:
            quad_points_num = np.array([quad_points], dtype=np.int32)
            cv2.fillPoly(mask, quad_points_num, 255)
            # Check is the finger inside the mask area
            if mask[int(y1), int(x1)] == 255:
                return True
    return False


def update_known_positions(corners, ids):
    for i, corner in enumerate(corners):
        id = ids[i][0]
        if id in [80, 160, 100, 180]:
            known_positions_global[id] = corner[0][np.argmin(corner[0][:, 0])]  # Update the known position


def ARTag_Detect(image2, width, height):

    # Turn the image to the grayscale
    gray = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
    corners, ids, rejectedImgPoints = detector.detectMarkers(gray)
    hand_inPosition = 0

    if ids is not None:
        update_known_positions(corners, ids)

        if len(known_positions_global) >= 3:
            if len(known_positions_global) == 3:
                estimate_positions = estimate_position.estimate_position3_method(known_positions_global)
                known_positions_global.update(estimate_positions)

            if len(known_positions_global) == 4:
                # Define the four points using the known positions
                tag_order = [80, 180, 160, 100]
                quad_points = np.array([known_positions_global[tag_id] for tag_id in tag_order], dtype=np.int32)
                quad_points = quad_points.reshape((-1, 1, 2))

                # Connect the points
                for i in range(len(quad_points)):
                    start_point = tuple(quad_points[i][0])
                    end_point = tuple(quad_points[(i + 1) % len(quad_points)][0])
                    cv2.line(image2, start_point, end_point, (255, 0, 0), thickness=2)

                if is_finger_inside_quad(image2, quad_points, hand_detector):
                    hand_inPosition = 1

    return image2, width, height, hand_inPosition

