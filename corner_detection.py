# -*- coding: UTF-8 -*- #
"""
@fileName:corner_detection.py
@author:Zicheng Wang
@time:2023-12-18
"""
import cv2
import numpy as np

import perspective_transform


def distance_calcu(point1, point2):
    x_coord = (point1[0] - point2[0]) ** 2
    y_coord = (point1[1] - point2[1]) ** 2
    result = np.sqrt(x_coord + y_coord)
    return result


def harris_corner_detect(image, prev_positions, detector, p_x, p_y, paint_content):
    corners = [None] * 4

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    rois = [
        (25, 600, 20, 20),
        (1229, 624, 20, 20),
        (1032, 57, 20, 20),
        (250, 60, 20, 20)
    ]

    rois2 = [
        (180, 500, 30, 25),  # Top left
        (1070, 513, 30, 25),  # Top right
        (974, 128, 30, 25),  # Bottom right
        (300, 122, 30, 25)  # Bottom left
    ]

    # Region of interest (ROI)
    for index, (x, y, w, h) in enumerate(rois2):
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)  # Draw the red part
        roi = gray[y:y + h, x:x + w]
        # Harris Corner detection
        corner_map = cv2.cornerHarris(roi, 2, 3, 0.04)
        # Get the highest strength of ROI
        _, max_strength, _, max_loc = cv2.minMaxLoc(corner_map)
        # Change the local to the global
        global_point = (max_loc[0] + x, max_loc[1] + y)

        # Check to keep the position stable
        if prev_positions is not None:
            prev_x, prev_y = prev_positions
            if abs(global_point[0] - prev_x) < 20 and abs(global_point[1] - prev_y) < 20:
                global_point = (prev_x, prev_y)

        corners[index] = global_point
        prev_positions = global_point  # Update the position

    # Draw lines between corners (No diagonal)
    for i in range(4):
        if i < 3:
            cv2.line(image, corners[i], corners[i + 1], (255, 0, 0), 2)
        else:
            cv2.line(image, corners[i], corners[0], (255, 0, 0), 2)

    # Print the corner
    for corner in corners:
        cv2.circle(image, corner, 5, (0, 255, 0), -1)
    hands, image = detector.findHands(image, draw=False)

    if hands:
        lmList = hands[0]['lmList']
        index_x1, index_y1, _ = lmList[8]
        thumb_tip = lmList[4]
        index_knuckle = lmList[7]

        draw_distance = distance_calcu(thumb_tip, index_knuckle)

        if draw_distance < 110:
            draw = 1
        else:
            draw = 0

        image, p_x, p_y = perspective_transform.perspective_transformation(image, corners, index_x1, index_y1, p_x, p_y, draw, paint_content)
    else:
        draw = 0
        image, p_x, p_y = perspective_transform.perspective_transformation(image, corners, 0, 0, p_x, p_y, draw, paint_content)

    # Show the image
    cv2.imshow('Corners Detected', image)

    return corners, prev_positions, p_x, p_y
