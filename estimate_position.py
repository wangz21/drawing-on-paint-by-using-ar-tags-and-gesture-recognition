# -*- coding: UTF-8 -*- #
"""
@fileName:estimate_position.py
@author:Zicheng Wang
@time:2023-12-09
"""

import numpy as np

# Store the relative position of each AR_Tag
adjacency_dict = {
    80: [100, 180],
    100: [80, 160],
    160: [100, 180],
    180: [80, 160]
}


# Similar triangle and vector to calculate
def estimate_position2_method(known_positions, rectangle_width, rectangle_height):
    estimated_positions = {}

    # Make sure we have two points
    if len(known_positions) == 2:
        # Extract the know position
        tags = list(known_positions.keys())
        pos1 = known_positions[tags[0]]
        pos2 = known_positions[tags[1]]
        # Check is the tag diagonal
        if tags[0] in adjacency_dict[tags[1]]:
            is_diagonal = False
        else:
            is_diagonal = True

        if is_diagonal:
            # Diagonal judgement
            diagonal_vector = pos2 - pos1
            diagonal_length = np.linalg.norm(diagonal_vector)
            diagonal_unit_vector = diagonal_vector / diagonal_length
            perp_vector = np.array([-diagonal_unit_vector[1], diagonal_unit_vector[0]])

            corner3 = pos1 + perp_vector * rectangle_height
            corner4 = pos2 + perp_vector * rectangle_height

            missing_tags = list(set(adjacency_dict.keys()) - set(tags))
            for missing_tag in missing_tags:
                if np.dot(adjacency_dict[missing_tag], diagonal_unit_vector) > 0:
                    estimated_positions[missing_tag] = corner4.tolist()
                else:
                    estimated_positions[missing_tag] = corner3.tolist()
        else:
            # Adjacency situation judgement
            delta = pos2 - pos1
            delta_length = np.linalg.norm(delta)
            delta_unit_vector = delta / delta_length

            # Perpendicular situation
            perp_vector = np.array([-delta_unit_vector[1], delta_unit_vector[0]])

            # Calculate the remaining situation
            if np.abs(delta[0]) > np.abs(delta[1]):  # Width direction
                corner3 = pos1 + perp_vector * rectangle_height
                corner4 = pos2 + perp_vector * rectangle_height
            else:  # Height direction
                corner3 = pos1 - delta_unit_vector * rectangle_width
                corner4 = pos2 - delta_unit_vector * rectangle_width

            missing_tags = list(set(adjacency_dict.keys()) - set(tags))
            for missing_tag in missing_tags:
                if tags[0] in adjacency_dict[missing_tag]:
                    adjacent_tag = tags[0]
                else:
                    adjacent_tag = tags[1]

                if adjacent_tag == tags[0]:
                    estimated_positions[missing_tag] = corner3.tolist() if missing_tag in adjacency_dict[
                        tags[1]] else corner4.tolist()
                else:
                    estimated_positions[missing_tag] = corner4.tolist() if missing_tag in adjacency_dict[
                        tags[0]] else corner3.tolist()
    return estimated_positions


# The method is better than using angle and distance of diagonal
def estimate_position3_method(known_positions):
    estimated_positions = {}

    # Make sure we have three tags
    # Find the unknown tags
    known_tags = set(known_positions.keys())
    all_tags = set(adjacency_dict.keys())
    missing_tag = list(all_tags - known_tags)[0]

    # Find the adjacency tag near the missing tag (only key without data)
    adjacent_tags = adjacency_dict[missing_tag]

    # Find two known tag key
    known_adjacent_tags = []
    for tag in adjacent_tags:
        if tag in known_positions:
            known_adjacent_tags.append(tag)

        # Extract the position of these two keys
    pos1 = known_positions[known_adjacent_tags[0]]
    pos2 = known_positions[known_adjacent_tags[1]]

    # Calculate the middle point of the two known tag
    mid_point = [(pos1[0] + pos2[0]) / 2.0, (pos1[1] + pos2[1]) / 2.0]

    third_tag = (known_tags - set(known_adjacent_tags)).pop()
    third_pos = known_positions[third_tag]

    # Use the third position to calculate
    estimated_x = 2 * mid_point[0] - third_pos[0]
    estimated_y = 2 * mid_point[1] - third_pos[1]

    # Return the position
    estimated_positions[missing_tag] = (estimated_x, estimated_y)

    return estimated_positions


'''
def calculate_angle_and_distance(pos1, pos2):
    #   Calculate the distance and angle between the two corners
    dx = pos2[0] - pos1[0]
    dy = pos2[1] - pos1[1]
    angle = np.arctan2(dy, dx)
    distance = np.sqrt(dx**2 + dy**2)
    return angle, distance


def estimate_position_method(known_position):
    estimated_positions = {}
    if len(known_position) == 3:
        # Find the mystery AR_TAG
        missing_tag = set(adjacency_dict.keys()) - set(known_position.keys())
        if len(missing_tag) == 1:
            missing_tag = missing_tag.pop()

        # Find the adjacency point (Two exist AR_TAG) of the missing tag.
        adjacent_tags = adjacency_dict[missing_tag]
        known_adjacent_tags = []
        for tag in adjacent_tags:
            if tag in known_position:
                known_adjacent_tags.append(tag)

        diagonal_tag = list(set(known_position.keys()) - set(known_adjacent_tags))[0]
        position_diagonal = known_position[diagonal_tag]

        if len(known_adjacent_tags) == 2:
            tag_id1, tag_id2 = known_adjacent_tags[:2]
            position1 = known_position[tag_id1]
            position2 = known_position[tag_id2]

            # Calculate the angle between these two point
            angle, distance = calculate_angle_and_distance(position1, position2)

            # Base on the diagonal to do the calculation
            # ERROR!!!!
            estimated_x = position_diagonal[0] + distance * np.cos(angle)
            estimated_y = position_diagonal[1] - distance * np.sin(angle)

            estimated_positions[missing_tag] = (estimated_x, estimated_y)
    return estimated_positions
'''
