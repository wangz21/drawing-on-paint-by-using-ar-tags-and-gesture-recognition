import numpy as np
import cv2
import matplotlib.pyplot as plt
from cvzone.HandTrackingModule import HandDetector
import time
import pyautogui

import ARTag_Detection
import corner_detection
import paint_recognition


def main():
    # To smooth the movement of mouse
    p_x, p_y = 0, 0

    # Get paint x & y:
    paint_content = []
    paint_content.append(paint_recognition.paint_recognition())

    # The another camera
    cap1 = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    cap1.set(cv2.CAP_PROP_FPS, 60)
    cap1.set(3, 1200)
    cap1.set(4, 1000)
    cap1.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))

    # The windows for the camera recorded
    cap2 = cv2.VideoCapture(1, cv2.CAP_DSHOW)  # 1 represent the external camera
    cap2.set(cv2.CAP_PROP_FPS, 60)
    cap2.set(3, 800)  # the wide of the screen is 1200 (3 represent the width)
    cap2.set(4, 720)  # the height of the screen is 720
    cap2.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))

    # For the Corner detection:
    prev_positions = None

    if not cap2.isOpened():
        print("Error: Camera not accessible.")
        exit()

    pTime = 0  # Set the starting point

    # 2. The method of receiving hand
    hand_detector = HandDetector(staticMode=False, maxHands=1, detectionCon=0.5, minTrackCon=0.2)
    width = 0
    height = 0

    frame_interval = 0.001
    last_process_time = time.time()
    # 3. Process every frame of the image
    while True:
        current_time = time.time()

        # If the image been received successfully
        success1, img1 = cap1.read()
        success2, img2 = cap2.read()

        # Rotate the image in the video, to make the reflection of oneself in the camera mirror each other
        img1 = cv2.flip(img1, flipCode=1)
        img2 = cv2.flip(img2, flipCode=1)  # 1 means horizontal rotate, and 0 means vertical rotate

        img2, width, height, hand_inPosition = ARTag_Detection.ARTag_Detect(img2, width, height)
        if hand_inPosition == 1:
            '''
            gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
            # Harris corner detection
            gray = np.float32(gray)
            dst = cv2.cornerHarris(gray, blockSize=2, ksize=3, k=0.04)

            # Result is dilated for marking the corners
            dst = cv2.dilate(dst, None)

            # Threshold for an optimal value, it may vary depending on the image
            img1[dst > 0.01 * dst.max()] = [0, 0, 255]

            # Convert the image with the drawn corners for displaying
            display_image_harris = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)

            cv2.imshow("image1", display_image_harris)
            '''
            corners, prev_positions, p_x, p_y = corner_detection.harris_corner_detect(img1, prev_positions,
                                                                                      hand_detector, p_x, p_y,
                                                                                      paint_content)

        last_process_time = current_time

        cTime = time.time()  # Get the current time
        fps = 1 / (cTime - pTime)
        pTime = cTime  # Set the start time to the current time

        cv2.putText(img2, str(int(fps)), (70, 50), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 0), 3)

        # cv2.imshow('image1', img1)
        cv2.imshow('image2', img2)
        if cv2.waitKey(1) & 0xFF == 27:
            break

    # release the video resource
    cap1.release()
    cap2.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
