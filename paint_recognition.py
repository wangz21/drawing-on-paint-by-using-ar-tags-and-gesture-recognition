# -*- coding: UTF-8 -*- #
"""
@fileName:paint_recognition.py
@author:Zicheng Wang
@time:2023-12-18
"""

import cv2
import numpy as np
from PIL import ImageGrab


def paint_recognition():
    # Get the screen shoot
    screenshot = ImageGrab.grab()
    screenshot_np = np.array(screenshot)

    # Transfer
    gray = cv2.cvtColor(screenshot_np, cv2.COLOR_BGR2GRAY)

    # Canny edge detection
    edges = cv2.Canny(gray, threshold1=50, threshold2=200)

    # Use the edge image to find the all contours and get the corner point of the paint app
    contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Find the biggest area of contours
    largest_contour = max(contours, key=cv2.contourArea)

    # Get the axis of the paint area
    x, y, w, h = cv2.boundingRect(largest_contour)

    # Return the coordinate
    return x, y, w, h

